#!/usr/bin/python

from flask import Flask, request, Response, json, abort
from sqlalchemy import *
import requests
#from flask_sqlalchemy import SQLAlchemy


app = Flask(__name__)
import time
time.sleep(4)

import bisect
import md5

class ConsistentHashRing(object):
    """Implement a consistent hashing ring."""

    def __init__(self, replicas=100):
        """Create a new ConsistentHashRing.

        :param replicas: number of replicas.

        """
        self.replicas = replicas
        self._keys = []
        self._nodes = {}

    def _hash(self, key):
        """Given a string key, return a hash value."""

        return long(md5.md5(key).hexdigest(), 16)

    def _repl_iterator(self, nodename):
        """Given a node name, return an iterable of replica hashes."""

        return (self._hash("%s:%s" % (nodename, i))
                for i in xrange(self.replicas))

    def __setitem__(self, nodename, node):
        """Add a node, given its name.

        The given nodename is hashed
        among the number of replicas.

        """
        for hash_ in self._repl_iterator(nodename):
            if hash_ in self._nodes:
                raise ValueError("Node name %r is "
                            "already present" % nodename)
            self._nodes[hash_] = node
            bisect.insort(self._keys, hash_)

    def __delitem__(self, nodename):
        """Remove a node, given its name."""

        for hash_ in self._repl_iterator(nodename):
            # will raise KeyError for nonexistent node name
            del self._nodes[hash_]
            index = bisect.bisect_left(self._keys, hash_)
            del self._keys[index]

    def __getitem__(self, key):
        """Return a node, given a key.

        The node replica with a hash value nearest
        but not less than that of the given
        name is returned.   If the hash of the
        given name is greater than the greatest
        hash, returns the lowest hashed node.

        """
        hash_ = self._hash(key)
        start = bisect.bisect(self._keys, hash_)
        if start == len(self._keys):
            start = 0
        return self._nodes[self._keys[start]]

# Consistent Hasing Ring
cr = ConsistentHashRing(1)
cr["node1"] = "rest1"
cr["node2"] = "rest2"
cr["node3"] = "rest3"

@app.route('/v1/expenses/<string:postID>', methods = ['GET', 'PUT', 'DELETE'])
def api_GET_PUT_DELETE(postID):
    if request.method == 'GET':

        server_node = cr[postID]
        API = "http://"+server_node+":5000/v1/expenses/"+postID
        result = requests.get(API)
        
        js = result.text

        if len(js) == 0:
            resp = Response("", status=404, mimetype='application/json')
            return resp
        else:
            resp = Response(js, status=200, mimetype='application/json')
            return resp


    elif request.method == 'PUT':
        #if request.headers['Content-Type'] == 'text/plain':
        #    js = request.data
        
        #check if the type is json
        #if request.headers['Content-Type'] == 'application/json':
        resp_dict = json.loads(request.data)
        #else:
        #    abort(404)
        server_node = cr[postID]
        API = "http://"+server_node+":5000/v1/expenses/"+postID
        result = requests.put(API, data=json.dumps(resp_dict))

        resp = Response(status=202)
        return resp
        


    elif request.method == 'DELETE':

        server_node = cr[postID]
        API = "http://"+server_node+":5000/v1/expenses/"+postID
        result = requests.delete(API)

        resp = Response(status=204)
        return resp

@app.route('/v1/expenses', methods = ['POST'])
def api_POST():
    if request.method == 'POST':    

        #if request.headers['Content-Type'] == 'text/plain':
        #   js = request.data
        #check if the type is json
        #if request.headers['Content-Type'] == 'application/json':
        resp_dict = json.loads(request.data)
        #else:
        #    abort(404)

        server_node = cr[resp_dict["id"]]
        API = "http://"+server_node+":5000/v1/expenses"
        result = requests.post(API, data=json.dumps(resp_dict))
        
        js = result.text

        #print js

        resp = Response(js, status=201, mimetype='application/json')
        
        #print "post_response = %s" %resp
        #resp = Response(js, status=201, mimetype='application/json')
        #resp.headers['Message'] = '201 OK'
        return resp




if __name__ == '__main__':
   app.run(host='0.0.0.0', debug=True)